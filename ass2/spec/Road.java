package ass2.spec;

import java.util.ArrayList;
import java.util.List;

/**
 * COMMENT: Comment Road 
 *
 * @author malcolmr
 */
public class Road {

    private List<Double> myPoints;
    private double myWidth;
    
    /** 
     * Create a new road starting at the specified point
     */
    public Road(double width, double x0, double y0) {
        myWidth = width;
        myPoints = new ArrayList<Double>();
        myPoints.add(x0);
        myPoints.add(y0);
    }

    /**
     * Create a new road with the specified spine 
     *
     * @param width
     * @param spine
     */
    public Road(double width, double[] spine) {
        myWidth = width;
        myPoints = new ArrayList<Double>();
        for (int i = 0; i < spine.length; i++) {
            myPoints.add(spine[i]);
        }
    }

    /**
     * The width of the road.
     * 
     * @return
     */
    public double width() {
        return myWidth;
    }

    /**
     * Add a new segment of road, beginning at the last point added and ending at (x3, y3).
     * (x1, y1) and (x2, y2) are interpolated as bezier control points.
     * 
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param x3
     * @param y3
     */
    public void addSegment(double x1, double y1, double x2, double y2, double x3, double y3) {
        myPoints.add(x1);
        myPoints.add(y1);
        myPoints.add(x2);
        myPoints.add(y2);
        myPoints.add(x3);
        myPoints.add(y3);        
    }
    
    /**
     * Get the number of segments in the curve
     * 
     * @return
     */
    public int size() {
        return myPoints.size() / 6;
    }

    /**
     * Get the specified control point.
     * 
     * @param i
     * @return
     */
    public double[] controlPoint(int i) {
        double[] p = new double[2];
        p[0] = myPoints.get(i*2);
        p[1] = myPoints.get(i*2+1);
        return p;
    }
    
    /**
     * Get a point on the spine. The parameter t may vary from 0 to size().
     * Points on the kth segment take have parameters in the range (k, k+1).
     * 
     * @param t
     * @return
     */
    public double[] point(double t) {
        int i = (int)Math.floor(t);
        t = t - i;
        
        i *= 6;
        
        double x0 = myPoints.get(i++);
        double y0 = myPoints.get(i++);
        double x1 = myPoints.get(i++);
        double y1 = myPoints.get(i++);
        double x2 = myPoints.get(i++);
        double y2 = myPoints.get(i++);
        double x3 = myPoints.get(i++);
        double y3 = myPoints.get(i++);
        
        double[] p = new double[2];

        p[0] = b(0, t) * x0 + b(1, t) * x1 + b(2, t) * x2 + b(3, t) * x3;
        p[1] = b(0, t) * y0 + b(1, t) * y1 + b(2, t) * y2 + b(3, t) * y3;        
        
        return p;
    }
    
    //Finds the normal to the curve at the point given by parameter t (Frenet frame calc)
    public double[] normal(double t) {
    	double[]n = new double[3];
    	
    	//Get the tangent (align with k axis)
    	//double[] tan = tangent(t);
    	double[] tan = approxTangent(t, 0.005);
    	
    	n[0] = -tan[1];
    	n[1] = tan[0];
		
		return n;
    }
    
    //Finds the tangent to the curve at the point given by parameter t
    public double[] tangent(double t) {
    	double[] tan = new double[] {0,0};
    	double[] pk1, pk;
    	
    	//Degree of Bezier curves
    	int m = 3;
    	
    	//Compute the tangent
    	for (int k = 0; k < m; k++) {
    		pk = controlPoint(k);
    		pk1 = controlPoint(k+1);
    		tan[0] += b(k, m-1) * (pk1[0] - pk[0]);
    		tan[1] += b(k, m-1) * (pk1[1] - pk[1]);
    	}
    	
    	tan[0] *= m;
    	tan[1] *= m;
    	
    	//Normalise it
    	double length = Math.sqrt(tan[0]*tan[0] + tan[1]*tan[1]);
    	
		if (length == 0) {
			return new double[]{0,0};
		}
		
		tan[0] = tan[0]/length;
		tan[1] = tan[1]/length;

    	return tan;
    }
    
    //Finds the tangent to the curve at the point given by parameter t
    public double[] approxTangent(double t, double dt) {
    	double[] tan = new double[] {0,0};
    	double[] p, pdt;
    	
    	double s = (double)size();
    	
    	if (t > (s - dt) && t < (s + dt)) {
    		p = point(Math.min(t-dt, s-dt));
        	pdt = point(Math.min(t, s-dt));
    	} else {
    		p = point(Math.min(t, s-dt));
        	pdt = point(Math.min(t+dt, s-dt));
    	}
    	
    	tan[0] = pdt[0] - p[0];
    	tan[1] = pdt[1] - p[1];
    	
    	//Normalise it
    	double length = Math.sqrt(tan[0]*tan[0] + tan[1]*tan[1]);
    	
		if (length == 0) {
			return new double[]{0,0};
		}
		
		tan[0] = tan[0]/length;
		tan[1] = tan[1]/length;

    	return tan;
    }
    
    /**
     * Calculate the Bezier coefficients
     * 
     * @param i
     * @param t
     * @return
     */
    private double b(int i, double t) {
        
        switch(i) {
        
        case 0:
            return (1-t) * (1-t) * (1-t);

        case 1:
            return 3 * (1-t) * (1-t) * t;
            
        case 2:
            return 3 * (1-t) * t * t;

        case 3:
            return t * t * t;
        }
        
        // this should never happen
        throw new IllegalArgumentException("" + i);
    }

    

}
