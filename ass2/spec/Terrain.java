package ass2.spec;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import com.jogamp.opengl.GL2;



/**
 * COMMENT: Comment HeightMap 
 *
 * @author malcolmr
 */
public class Terrain {

    private Dimension mySize;
    private double[][] myAltitude;
    private List<Tree> myTrees;
    private List<Road> myRoads;
    private List<Enemy> myEnemies;
    private float[] mySunlight;

    /**
     * Create a new terrain
     *
     * @param width The number of vertices in the x-direction
     * @param depth The number of vertices in the z-direction
     */
    public Terrain(int width, int depth) {
        mySize = new Dimension(width, depth);
        myAltitude = new double[width][depth];
        myTrees = new ArrayList<Tree>();
        myRoads = new ArrayList<Road>();
        myEnemies = new ArrayList<Enemy>();
        mySunlight = new float[3];
    }
    
    public Terrain(Dimension size) {
        this(size.width, size.height);
    }

    public Dimension size() {
        return mySize;
    }

    public List<Tree> trees() {
        return myTrees;
    }

    public List<Road> roads() {
        return myRoads;
    }
    
    public List<Enemy> enemies() {
        return myEnemies;
    }

    public float[] getSunlight() {
        return mySunlight;
    }

    /**
     * Set the sunlight direction. 
     * 
     * Note: the sun should be treated as a directional light, without a position
     * 
     * @param dx
     * @param dy
     * @param dz
     */
    public void setSunlightDir(float dx, float dy, float dz) {
        mySunlight[0] = dx;
        mySunlight[1] = dy;
        mySunlight[2] = dz;        
    }
    
    /**
     * Resize the terrain, copying any old altitudes. 
     * 
     * @param width
     * @param height
     */
    public void setSize(int width, int height) {
        mySize = new Dimension(width, height);
        double[][] oldAlt = myAltitude;
        myAltitude = new double[width][height];
        
        for (int i = 0; i < width && i < oldAlt.length; i++) {
            for (int j = 0; j < height && j < oldAlt[i].length; j++) {
                myAltitude[i][j] = oldAlt[i][j];
            }
        }
    }

    /**
     * Get the altitude at a grid point
     * 
     * @param x
     * @param z
     * @return
     */
    public double getGridAltitude(int x, int z) {
        return myAltitude[x][z];
    }

    /**
     * Set the altitude at a grid point
     * 
     * @param x
     * @param z
     * @return
     */
    public void setGridAltitude(int x, int z, double h) {
        myAltitude[x][z] = h;
    }

    /**
     * Get the altitude at an arbitrary point. 
     * Non-integer points should be interpolated from neighbouring grid points
     * 
     * TODO
     * 
     * @param x
     * @param z
     * @return
     */
    public double altitude(double x, double z) {
        
        //Return 0 for coordinates outside the terrain
        if ((x < 0 || x > mySize.getWidth() - 1) || (z < 0 || z > mySize.getHeight() - 1)) {
        	return 0;
        }
        
        //Use the direct values for grid points
        if (x == (int) x && z == (int) z) {
        	return myAltitude[(int) x][(int) z];
        }
        
        //Interpolate for intermediate values
        double lowerleft = myAltitude[(int) Math.floor(x)][(int) Math.floor(z)];
        double lowerright = myAltitude[(int) Math.ceil(x)][(int) Math.floor(z)];
        double upperleft = myAltitude[(int) Math.floor(x)][(int) Math.ceil(z)];
        double upperright = myAltitude[(int) Math.ceil(x)][(int) Math.ceil(z)];
        
        double dx = x - Math.floor(x);
        double dz = z - Math.floor(z);
        
        return lowerleft + (lowerright-lowerleft)*dx + (upperleft-lowerleft)*dz + (lowerleft-lowerright-upperleft+upperright)*dx*dz;
    }

    /**
     * Add a tree at the specified (x,z) point. 
     * The tree's y coordinate is calculated from the altitude of the terrain at that point.
     * 
     * @param x
     * @param z
     */
    public void addTree(double x, double z) {
        double y = altitude(x, z);
        Tree tree = new Tree(x, y, z);
        myTrees.add(tree);
    }


    /**
     * Add a road. 
     * 
     * @param x
     * @param z
     */
    public void addRoad(double width, double[] spine) {
        Road road = new Road(width, spine);
        myRoads.add(road);        
    }
    
    /**
     * Add an enemy. 
     * 
     * @param x
     * @param z
     */
    public void addEnemy(double x, double z) {
    	double y = altitude(x, z);
        Enemy enemy = new Enemy(x, y, z);
        myEnemies.add(enemy);        
    }
    
    public void drawTerrain(GL2 gl) {
    	gl.glBegin(GL2.GL_TRIANGLES);
    	for (int i = 0; i < myAltitude.length - 1; i++) {
    		for (int j = 0; j < myAltitude.length - 1; j++) {
    			gl.glVertex3d(i, myAltitude[i][j], j);
    			gl.glVertex3d(i+1, myAltitude[i+1][j], j);
    			gl.glVertex3d(i, myAltitude[i][j+1], j+1);
    		}
    	}
    	for (int i = 1; i < myAltitude.length; i++) {
    		for (int j = 1; j < myAltitude.length; j++) {
    			gl.glVertex3d(i, myAltitude[i][j], j);
    			gl.glVertex3d(i-1, myAltitude[i-1][j], j);
    			gl.glVertex3d(i, myAltitude[i][j-1], j-1);
    		}
    	}
    	gl.glEnd();
    	
    }


}