#version 130

in vec4 vertexCol;
in vec4 vertexPos;
//in vec4 vertexNor;

out vec4 fragCol;

void main(void) {
    fragCol = vertexCol;
	gl_Position=gl_ModelViewProjectionMatrix*vertexPos;
    gl_FrontColor = vertexCol;  
}


