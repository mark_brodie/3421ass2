package ass2.spec;

import ass2.spec.Shader.CompilationException;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.FloatBuffer;

import javax.swing.JFrame;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLJPanel;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.gl2.GLUT;


/**
 * COMMENT: Comment Game 
 *
 * @author malcolmr
 */
public class Game extends JFrame implements GLEventListener, KeyListener, MouseMotionListener {

	private Terrain myTerrain;

	//Boolean toggles for key events
	private boolean lighting = true;
	private boolean sun = true;
	private boolean ambient = true;
	private boolean backFaces = false;
	private boolean avatar = false;
	private boolean night = false;
	private boolean torch = false;
	
	//Global ambient light level
	private float[] dark = new float[]{0f, 0f, 0f, 1.0f};
	private float[] amb = new float[]{0.4f, 0.6f, 0.8f, 1.0f};
	private float[] ambNight = new float[]{0.1f, 0.2f, 0.3f, 1.0f};
	
	//Variables for controlling the camera
	private double nearPlane = 0.05;
	private double focalDistance = 20;
	private double[] camPos = new double[] {0,0,0};
	private double[] camDir = new double[] {0,0,1};
	private float[] torchPos = new float[] {0f,0f,0f};
	private float[] torchDir = new float[] {0f,0f,0f};
	private double angleXZ = (Math.PI/2);
	private double distancestep = 0.1;
	private double anglestep = 0.1;
	double avatarDist = 0.75;
	
	//VBO variables
	private FloatBuffer enemyPos;
	private FloatBuffer enemyCol;
	private FloatBuffer enemyNor;
	private int bufferIds[] = new int[2];
	float[] ePos;
	float[] eCol; 
	float[] eNor;
	int maxStacks = 32, maxSlices = 32;
	
	//Shaders
	private static final String VERTEX_SHADER = "ass2/spec/VertexShader.glsl";
	private static final String FRAGMENT_SHADER = "ass2/spec/FragmentShader.glsl";
	private int shaderprogram;

	//Textures
	private Texture myTextures[];
	private String leavesTexture = "ass2/spec/leaves.bmp";
	private String roadTexture = "ass2/spec/road.bmp";
	private String terrainTexture = "ass2/spec/terrain.bmp";
	private String trunkTexture = "ass2/spec/trunk.bmp";
	private String teapotTexture = "ass2/spec/teapot.bmp";
	
    public Game(Terrain terrain) {
    	super("Assignment 2");
        myTerrain = terrain;
   
    }
    
    /** 
     * Run the game.
     *
     */
    public void run() {
    	  GLProfile glp = GLProfile.getDefault();
          GLCapabilities caps = new GLCapabilities(glp);
          GLJPanel panel = new GLJPanel();
          panel.addGLEventListener(this);
          panel.addKeyListener(this);
 
          // Add an animator to call 'display' at 60fps        
          FPSAnimator animator = new FPSAnimator(60);
          animator.add(panel);
          animator.start();

          getContentPane().add(panel);
          setSize(800, 600);        
          setVisible(true);
          setDefaultCloseOperation(EXIT_ON_CLOSE);        
    }
    
    /**
     * Load a level file and display it.
     * 
     * @param args - The first argument is a level file in JSON format
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        Terrain terrain = LevelIO.load(new File(args[0]));
        Game game = new Game(terrain);
        game.run();
    }

	@Override
	public void display(GLAutoDrawable drawable) {
		
		//Debug
		//drawable.setGL(new DebugGL2(drawable.getGL().getGL2()));
        //drawable.setGL(new DebugGL2(new TraceGL2(drawable.getGL().getGL2(), System.err)));
		
        GL2 gl = drawable.getGL().getGL2();

        //Clear buffers
        gl.glClear(GL.GL_COLOR_BUFFER_BIT);
        gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
        
        //Reset model view matrix
        gl.glMatrixMode(GL2.GL_MODELVIEW);
    	gl.glLoadIdentity();

    	//Enable lights
    	if (lighting) {
    		gl.glEnable(GL2.GL_LIGHTING); 
    		
    		if (sun) {
    			gl.glEnable(GL2.GL_LIGHT0);
    	        //gl.glDisable(GL2.GL_LIGHT1);
    		} else {
    			//gl.glEnable(GL2.GL_LIGHT1);
    	        gl.glDisable(GL2.GL_LIGHT0);
    		}
    		
    		if (ambient) {	
    			gl.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, amb, 0);
    		} else if (night) {
    			gl.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, ambNight, 0);
    		} else {
    			gl.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, dark, 0);
    		}
    		
    		if (torch) {
    			gl.glEnable(GL2.GL_LIGHT2);
    		} else {
    			gl.glDisable(GL2.GL_LIGHT2);
    		}
    		
    	} else {
    		gl.glDisable(GL2.GL_LIGHTING);
    	}
    	
    	if (!backFaces) {
    		gl.glEnable(GL2.GL_CULL_FACE);
    	} else { 
    		gl.glDisable(GL2.GL_CULL_FACE);
    	}
    	
    	//Setup Camera
    	GLU glu = new GLU();
        double lookAt[] = new double[] {camPos[0]+camDir[0]*focalDistance, camPos[1]+camDir[1]*focalDistance, camPos[2]+camDir[2]*focalDistance};
        glu.gluLookAt(camPos[0],camPos[1],camPos[2],lookAt[0],lookAt[1],lookAt[2],0,1,0);
    	
        //Draw the terrain
        drawTerrain(gl);
        //myTerrain.drawTerrain(gl);
        
        //Draw the trees
        drawTrees(gl);
        
        //Draw the roads
        drawRoads(gl);
        
        //Draw the avatar if needed
        if (avatar) {
        	drawAvatar(gl);
        }
        
        //Draw enemies
        //drawEnemies(gl);
        renderEnemies(gl);
        
        //Position the torch
		gl.glPushMatrix();
		
		//Setup the coordinate frame
		gl.glTranslated((camPos[0]-camDir[0]*avatarDist), (camPos[1]+camDir[1]*avatarDist) + 0.1, (camPos[2]-camDir[2]*avatarDist));
		gl.glRotated((-angleXZ/Math.PI*180), 0, 1, 0);
		
		gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_SPOT_DIRECTION, torchDir, 0);
		gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_POSITION, torchPos, 0);
		
		gl.glPopMatrix();
        
        //Set mode back to fill just in case
        gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2.GL_FILL);
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
        gl.glDeleteBuffers(1,bufferIds,0);
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		
		GL2 gl = drawable.getGL().getGL2();

        gl.glClearColor(1.0f, 1.0f, 1.0f, 1f);
		//gl.glClearColor(0,0,0,1f);
        gl.glEnable(GL2.GL_DEPTH_TEST);  
        
        //Enable basic lighting (Sun)
        gl.glEnable(GL2.GL_LIGHTING);
        //gl.glEnable(GL2.GL_LIGHT0);
        //gl.glEnable(GL2.GL_LIGHT1);
        gl.glEnable(GL2.GL_LIGHT2);
        
        //Setup the sunlight vector
        float[] sunVector = new float[] {myTerrain.getSunlight()[0], myTerrain.getSunlight()[1], myTerrain.getSunlight()[2], 0};
        float[] diff = { 1.0f, 1.0f, 1.0f, 1.0f };
        float[] spec = { 1.0f, 1.0f, 1.0f, 1.0f };
        
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, sunVector, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, diff,0);
    	gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, spec,0);
        
    	gl.glLightModelfv(GL2.GL_LIGHT_MODEL_AMBIENT, amb, 0);
    	gl.glLightModeli(GL2.GL_LIGHT_MODEL_TWO_SIDE, GL2.GL_TRUE);
    	
    	//Torch
    	gl.glLightf(GL2.GL_LIGHT2, GL2.GL_SPOT_CUTOFF, 60);
    	gl.glLightf(GL2.GL_LIGHT2, GL2.GL_SPOT_EXPONENT, 4);
    	gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_DIFFUSE, diff,0);
    	gl.glLightfv(GL2.GL_LIGHT2, GL2.GL_SPECULAR, spec,0);
    	
        //Enable normalisation of lighting normals
        gl.glEnable(GL2.GL_NORMALIZE);
        
        //Back Face Culling
        //gl.glEnable(GL2.GL_CULL_FACE);
        gl.glCullFace(GL2.GL_BACK);
        
        //Setup the enemy VBOs
        setupEnemies(gl);  
        
        //Shaders
        try {
   		 	shaderprogram = Shader.initShaders(gl,VERTEX_SHADER,FRAGMENT_SHADER);	 
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        //Textures
        myTextures = new Texture[5];
    	myTextures[0] = new Texture(gl,leavesTexture,"bmp",true);
    	myTextures[1] = new Texture(gl,roadTexture,"bmp",true);
    	myTextures[2] = new Texture(gl,terrainTexture,"bmp",true);
    	myTextures[3] = new Texture(gl,trunkTexture,"bmp",true);
    	myTextures[4] = new Texture(gl,teapotTexture,"bmp",true);
    	
    	gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_MODULATE); 
    	gl.glEnable(GL2.GL_TEXTURE_2D); 
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

		GL2 gl = drawable.getGL().getGL2();
		
		//Reset projection matrix
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();

        GLU glu = new GLU();
        
        //Orthographic Camera
        //gl.glOrtho(-10, 10, -10, 10, 1, 20);
        
        //Perspective camera
        glu.gluPerspective(60.0, (float)width/(float)height, nearPlane, focalDistance);
        //gl.glFrustum(-2,2,-2,2,2,20); 
        
        //Look at
        double lookAt[] = new double[] {camPos[0]+camDir[0]*focalDistance, camPos[1]+camDir[1]*focalDistance, camPos[2]+camDir[2]*focalDistance};
        glu.gluLookAt(camPos[0],camPos[1],camPos[2],lookAt[0],lookAt[1],lookAt[2],0,1,0);
	}
	
	//Function to draw the terrain from the loaded level file
	public void drawTerrain(GL2 gl) {
		
		//Set back colour to blue (lines)
		gl.glPolygonMode(GL.GL_BACK, GL2.GL_LINE);
		gl.glColor3f(0.1f,0.1f,1f);
		
		//Set front colour to brown
		gl.glPolygonMode(GL.GL_FRONT, GL2.GL_FILL);
		gl.glColor3f(0.6f,0.2f,0f);

		//Set lighting properties
		float[] ambdiff = {0.5f, 0.8f, 0.1f, 1.0f};
		float[] spec = {0.1f, 0.1f, 0.1f, 1.0f};
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, ambdiff, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, spec, 0);
		
		
		gl.glBindTexture(GL2.GL_TEXTURE_2D, myTextures[2].getTextureId());
		gl.glBegin(GL2.GL_TRIANGLES);
		{			
			// Draw terrain triangles in format:
			// +-+-+
			// |/|/|      A-B
			// +-+-+ ==>  |/|
			// |/|/|      C-D
			// +-+-+
    		
			for (int i = 0; i < this.myTerrain.size().width - 1; i++) {
				for (int j = 0; j < this.myTerrain.size().height - 1; j++) {
					
					double x = (double)i;
					double z = (double)j;

					double[] A = new double[] {x, myTerrain.altitude(x, z), z};
					double[] B = new double[] {x+1, myTerrain.altitude(x+1, z), z};
					double[] C = new double[] {x, myTerrain.altitude(x, z+1), z+1};
					double[] D = new double[] {x+1, myTerrain.altitude(x+1, z+1), z+1};
					
					//Top Left
					//double[] n1 = calcFaceNormal(A, C, B);
					double[] n1 = calcNewellNormal(A, C, B);
					gl.glNormal3d(n1[0], n1[1], n1[2]);
					gl.glVertex3dv(A, 0);
					gl.glTexCoord2d(1, 0);
					gl.glVertex3dv(C, 0);
					gl.glTexCoord2d(0, 0);
					gl.glVertex3dv(B, 0);					
					gl.glTexCoord2d(1, 1);
					
					//Bottom Right
					//double[] n2 = calcFaceNormal(C, D, B);
					double[] n2 = calcNewellNormal(C, D, B);
					gl.glNormal3d(n2[0], n2[1], n2[2]);
					gl.glVertex3dv(C, 0);
					gl.glTexCoord2d(0, 0);
					gl.glVertex3dv(D, 0);
					gl.glTexCoord2d(0, 1);
					gl.glVertex3dv(B, 0);
					gl.glTexCoord2d(1, 1);
				}
			}
    	}
		gl.glEnd(); 
		
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2.GL_FILL);
	}
	
	//Draw all trees on the terrain
	public void drawTrees(GL2 gl) {
	
		int numSegments = 8;
		double trunkrad = 0.05;
		double leavesrad = 0.25;
		double height = 0.75;
		double theta = 0;
		double angleStep = 2*Math.PI/numSegments;
		
		//Set colour to purple
		gl.glColor3f(0.5f,0,0.5f);
		
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2.GL_FILL);
		
		for (Tree tr : this.myTerrain.trees()) {
			
			//Get elevation at this point
			double[] pos = tr.getPosition();
			
			//Set lighting properties
			float[] t_ambdiff = {0.6f, 0.2f, 0.0f, 1.0f};
			float[] t_spec = {0.1f, 0.1f, 0.1f, 1.0f};
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, t_ambdiff, 0);
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, t_spec, 0);
			
			gl.glPushMatrix();
			gl.glTranslated(pos[0], pos[1], pos[2]);
			
			gl.glBindTexture(GL2.GL_TEXTURE_2D, myTextures[3].getTextureId());
			
			//Top circle
	    	gl.glBegin(GL2.GL_TRIANGLE_FAN);
	    	{
	    		 gl.glNormal3d(0,1,0);
	    		 gl.glVertex3d(0, height, 0);

	    		 theta = 0;
	             for (int i = 0; i < numSegments + 1; i++) {
	            	 gl.glTexCoord2d((trunkrad/2) + (trunkrad/2)*Math.cos(theta),(trunkrad/2) + (trunkrad/2)*Math.sin(theta));
	                 gl.glVertex3d(Math.sin(theta) * trunkrad, height, Math.cos(theta) * trunkrad);
	                 theta += angleStep;
	             }                
	    	}
	    	gl.glEnd();
	    	
	    	//Bottom circle
	    	gl.glBegin(GL2.GL_TRIANGLE_FAN);
	    	{
	   		 	gl.glNormal3d(0,-1,0);
	   		 	gl.glVertex3d(0,0,0);
	   		 	
	   		 	theta = 0;
	            for (int i = 0; i < numSegments + 1; i++) {
	            	gl.glTexCoord2d((trunkrad/2) + (trunkrad/2)*Math.cos(theta),(trunkrad/2) + (trunkrad/2)*Math.sin(theta));
	                gl.glVertex3d(Math.sin(theta) * trunkrad, 0, Math.cos(theta) * trunkrad);
	                theta += angleStep;
	            }       
	    	}
	    	gl.glEnd();
	    	  
	    	//Sides of the cylinder
	    	gl.glBegin(GL2.GL_QUAD_STRIP);
	        {
	        	theta = 0;
	            for (int i = 0; i < numSegments + 1; i++){
	            	gl.glNormal3d(trunkrad * Math.cos(theta), 0, trunkrad * Math.sin(theta));
	            	gl.glTexCoord2d(((float)i/numSegments), 0);
	            	gl.glVertex3d(trunkrad * Math.cos(theta), 0, trunkrad * Math.sin(theta));
	            	gl.glTexCoord2d(((float)i/numSegments), 1);
	                gl.glVertex3d(trunkrad * Math.cos(theta), height, trunkrad * Math.sin(theta));
	                theta += angleStep;
	            }
	        }
	        gl.glEnd();
	        
	        //Draw the leaves
	        gl.glBindTexture(GL2.GL_TEXTURE_2D, myTextures[0].getTextureId());
			gl.glTranslated(0, leavesrad/2 + height, 0);
			
			//Set lighting properties
			float[] l_ambdiff = {0.0f, 0.6f, 0.0f, 1.0f};
			float[] l_spec = {0.15f, 0.15f, 0.15f, 1.0f};
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, l_ambdiff, 0);
			gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, l_spec, 0);
			
			/*
			for(int i = 0; i <= numSegments; i++) {
				double lat0 = Math.PI * (-0.5 + (double) (i - 1) / numSegments);
				double z20  = Math.sin(lat0)*leavesrad;
				double zr0 =  Math.cos(lat0)*leavesrad;

				double lat1 = Math.PI * (-0.5 + (double) i / numSegments);
				double z21 = Math.sin(lat1)*leavesrad;
				double zr1 = Math.cos(lat1)*leavesrad;
				
				for (int k=0; k<365; k++) {
					gl.glBegin(GL2.GL_QUAD_STRIP);
					for(int j = 0; j <= numSegments; j++) {
						double lng = 2 * Math.PI * (double) (j - 1) / numSegments;
						double x = Math.cos(lng)*leavesrad;
						double y = Math.sin(lng)*leavesrad;

						gl.glNormal3d(x * zr0, y * zr0, z20);
						gl.glVertex3d(x * zr0, y * zr0, z20);
						gl.glNormal3d(x * zr1, y * zr1, z21);
						gl.glVertex3d(x * zr1, y * zr1, z21);
					}
					gl.glEnd();
					gl.glRotated(1, 1, 0, 0);
				}	
			}
			*/

			double deltaT = (leavesrad*2)/maxStacks;
			double deltaTheta = (Math.PI*2)/maxSlices;
			double t,x1,x2,z1,z2,y1,y2,sth,cth,ct,cta;
			
			//Using only 2 for loops (parameterised sphere function)
			gl.glBegin(GL2.GL_TRIANGLE_STRIP);
    		{
				for (int i = 0; i < maxStacks; i++)  { 
					
		    		t = (-(leavesrad) + 2*i*deltaT) * 2 * Math.PI;

		    		for(int j = 0; j <= maxSlices; j++)   {  
		    			
		    			theta = j*deltaTheta;
		    			
		    			sth = Math.sin(theta);
		    			cth = Math.cos(theta);
		    			ct = Math.cos(t);
		    			cta = Math.cos(t + deltaTheta);
		    			
		    			x1 = leavesrad * ct * cth; 
		    			x2 = leavesrad * cta * cth; 
		    			y1 = leavesrad * Math.sin(t);
		    			y2 = leavesrad * Math.sin(t + deltaTheta);
		    			z1 = leavesrad * ct * sth;  
		    			z2 = leavesrad * cta * sth;
		    			
		    			double n1[] = normalise(new double[]{x1,y1,z1});
		    			double n2[] = normalise(new double[]{x2,y2,z2});
	
		    			gl.glNormal3dv(n1, 0);
		    			gl.glTexCoord2d((1.0/maxSlices) * j, 1.0/maxStacks * i);
		    			gl.glVertex3d(x1,y1,z1); 
		    			gl.glNormal3dv(n2,0); 
		    			gl.glTexCoord2d((1.0/maxSlices) * j, (1.0/maxStacks) * (i+1));
		    			gl.glVertex3d(x2,y2,z2); 
		    		}
	    		}
	    	}
    		gl.glEnd();
			
			gl.glPopMatrix();
		}
		
	}
	
	//Draw all roads on the terrain
	public void drawRoads(GL2 gl) {
		
		int roadSegments = 32;
		double altoffset = 0.005;
		double s,w,t,alt,x1,z1,x2,z2;
		double[] p, n;
		
		//Set colour to yellow
		gl.glColor3f(1,1,0);
		
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2.GL_FILL);
		
		//Set lighting properties
		float[] ambdiff = {0.05f, 0.05f, 0.05f, 1.0f};
		float[] spec = {0.1f, 0.1f, 0.1f, 1.0f};
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, ambdiff, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, spec, 0);
		
		gl.glBindTexture(GL2.GL_TEXTURE_2D, myTextures[1].getTextureId());
		
		for (Road r : this.myTerrain.roads()) {

			w = r.width();
			s = (double)r.size();

			for (int spline = 0; spline < s; spline++) {
				
				gl.glBegin(GL2.GL_QUAD_STRIP);
	    		{
	    			for (int i = 0; i < roadSegments; i++) {
					
						t = (double)i/roadSegments + (double)spline;
						
						p = r.point(t);
		    			n = r.normal(t);
		    			alt = myTerrain.altitude(p[0], p[1]);
		    			
		    			x1 = p[0] - (w/2 * n[0]);
		    			z1 = p[1] - (w/2 * n[1]);
		    			x2 = p[0] + (w/2 * n[0]);
		    			z2 = p[1] + (w/2 * n[1]);
		    			
		    			gl.glNormal3d(0,1,0);
		    			//gl.glVertex3d(x1, myTerrain.altitude(x1, z1)+altoffset, z1);
		    			//gl.glVertex3d(x2, myTerrain.altitude(x2, z2)+altoffset, z2);
		    			gl.glTexCoord2d(0, 0);
		    			gl.glVertex3d(x1, alt+altoffset, z1);
		    			gl.glTexCoord2d(0, 0.1);
		    			gl.glVertex3d(x2, alt+altoffset, z2);
		    		}
    			
	    			//Use for the last two points (value needs to be close to 1 but not equal)
	    			p = r.point(spline+0.999);
	    			n = r.normal(spline+0.999);
	    			alt = myTerrain.altitude(p[0], p[1]);
	    			
	    			x1 = p[0] - (w/2 * n[0]);
	    			z1 = p[1] - (w/2 * n[1]);
	    			x2 = p[0] + (w/2 * n[0]);
	    			z2 = p[1] + (w/2 * n[1]);
	    			
	    			gl.glNormal3d(0, 1, 0);
	    			gl.glVertex3d(x1, alt+altoffset, z1);
	    			gl.glTexCoord2d(0, 1);
	    			gl.glVertex3d(x2, alt+altoffset, z2);
	    			gl.glTexCoord2d(1, 1);
				}
	    		gl.glEnd();	
			}
		}
	}
	
	//Draw the avatar
	public void drawAvatar(GL2 gl) {
		
		//Set colour to blue
		gl.glColor3f(0,0,1);
		
		//Set lighting properties
		float[] ambdiff = {0.3f, 0.3f, 0.3f, 1.0f};
		float[] spec = {0.1f, 0.4f, 0.9f, 1.0f};
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, ambdiff, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, spec, 0);
		
		gl.glPushMatrix();
		
		//Setup the coordinate frame
		gl.glTranslated((camPos[0]-camDir[0]*avatarDist), (camPos[1]+camDir[1]*avatarDist) + 0.1, (camPos[2]-camDir[2]*avatarDist));
		gl.glRotated((-angleXZ/Math.PI*180)+180, 0, 1, 0);
		
		gl.glBindTexture(GL2.GL_TEXTURE_2D, myTextures[4].getTextureId());
		
		GLUT glut = new GLUT();
		gl.glFrontFace(GL2.GL_CW);
        glut.glutSolidTeapot(0.1);
        gl.glFrontFace(GL2.GL_CCW);
		
		gl.glPopMatrix();
	}
	
	//Sets up the enemies in the needed VBOs
	public void setupEnemies(GL2 gl) {

		int index = 0, nEnemies;
		double theta = 0, rad = 0.25;
		double deltaT = (rad*2)/maxStacks;
		double deltaTheta = (Math.PI*2)/maxSlices;
		double t,x1,x2,z1,z2,y1,y2,sth,cth,ct,cta;
		
		nEnemies = myTerrain.enemies().size();
		ePos = new float[nEnemies * maxStacks * (maxSlices+1) * 2 * 3];
		eCol = new float[nEnemies * maxStacks * (maxSlices+1) * 2 * 3];
		eNor = new float[nEnemies * maxStacks * (maxSlices+1) * 2 * 3];
		
		for (int i = 0; i < nEnemies * maxStacks * (maxSlices+1) * 2 * 3; i++) {
			ePos[i] = 0f;
			eCol[i] = 0f;
			eNor[i] = 0f;
		}
		
		for (Enemy e : myTerrain.enemies()) {
			
			double[] pos = e.getPosition();
			
			//Draw enemies as spheres
			for (int i = 0; i < maxStacks; i++)  { 
				
				t = (-(rad) + 2*i*deltaT) * 2 * Math.PI;

	    		for(int j = 0; j <= maxSlices; j++)   {  
	    			
	    			theta = j*deltaTheta;
	    			
	    			sth = Math.sin(theta);
	    			cth = Math.cos(theta);
	    			ct = Math.cos(t);
	    			cta = Math.cos(t + deltaTheta);
	    			
	    			x1 = rad * ct * cth; 
	    			x2 = rad * cta * cth; 
	    			y1 = rad * Math.sin(t);
	    			y2 = rad * Math.sin(t + deltaTheta);
	    			z1 = rad * ct * sth;  
	    			z2 = rad * cta * sth;
	    			
	    			double n1[] = normalise(new double[]{x1,y1,z1});
	    			double n2[] = normalise(new double[]{x2,y2,z2});

	    			ePos[index] = (float)(x1+pos[0]);
	    			eNor[index] = (float)n1[0];
	    			eCol[index++] = (float) Math.random();
	    			ePos[index] = (float)(y1+pos[1]+rad);
	    			eNor[index] = (float)n1[1];
	    			eCol[index++] = (float) Math.random();
	    			ePos[index] = (float)(z1+pos[2]);
	    			eNor[index] = (float)n1[2];
	    			eCol[index++] = (float) Math.random();

	    			ePos[index] = (float)(x2+pos[0]);
	    			eNor[index] = (float)n2[0];
	    			eCol[index++] = (float) Math.random();
	    			ePos[index] = (float)(y2+pos[1]+rad);
	    			eNor[index] = (float)n2[1];
	    			eCol[index++] = (float) Math.random();
	    			ePos[index] = (float)(z2+pos[2]);
	    			eNor[index] = (float)n2[2];
	    			eCol[index++] = (float) Math.random();
	    		}
	    	}
		}
		
		enemyPos = Buffers.newDirectFloatBuffer(ePos);
		enemyCol = Buffers.newDirectFloatBuffer(eCol);
		enemyNor = Buffers.newDirectFloatBuffer(eNor);
		
		gl.glGenBuffers(1,bufferIds,0);
        gl.glBindBuffer(GL.GL_ARRAY_BUFFER,bufferIds[0]);
        //gl.glBufferData(GL2.GL_ARRAY_BUFFER, (ePos.length + eCol.length) * Float.BYTES, null, GL2.GL_STATIC_DRAW);
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, (ePos.length + eCol.length + eNor.length) * Float.BYTES, null, GL2.GL_STATIC_DRAW);
        
        gl.glBufferSubData(GL2.GL_ARRAY_BUFFER, 0, ePos.length * Float.BYTES, enemyPos);
        gl.glBufferSubData(GL2.GL_ARRAY_BUFFER, ePos.length * Float.BYTES, eCol.length * Float.BYTES, enemyCol);
        gl.glBufferSubData(GL2.GL_ARRAY_BUFFER, (ePos.length + eCol.length) * Float.BYTES, eNor.length * Float.BYTES, enemyNor);
	}
	
	//Render the enemies using VBOs
	public void renderEnemies(GL2 gl) {
		
		//Set lighting properties
		float[] ambdiff = {1f, 0.2f, 0.0f, 1.0f};
		float[] spec = {0.9f, 0.1f, 0.09f, 1.0f};
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, ambdiff, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, spec, 0);
		
		gl.glUseProgram(shaderprogram);
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER,bufferIds[0]);
		
		int vertexColLoc = gl.glGetAttribLocation(shaderprogram,"vertexCol");
        int vertexPosLoc = gl.glGetAttribLocation(shaderprogram,"vertexPos");
        //int vertexNorLoc = gl.glGetAttribLocation(shaderprogram, "vertexNor");
        
        gl.glEnableVertexAttribArray(vertexPosLoc);
        gl.glEnableVertexAttribArray(vertexColLoc);
        //gl.glEnableVertexAttribArray(vertexNorLoc);
        
        gl.glVertexAttribPointer(vertexPosLoc, 3, GL.GL_FLOAT, false, 0, 0); 
        gl.glVertexAttribPointer(vertexColLoc, 3, GL.GL_FLOAT, false, 0, ePos.length * Float.BYTES); 
        //gl.glVertexAttribPointer(vertexPosLoc, 3, GL.GL_FLOAT, false, 0, (ePos.length + eCol.length) * Float.BYTES); 
        
		gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL2.GL_COLOR_ARRAY);
        gl.glEnableClientState(GL2.GL_NORMAL_ARRAY);
        
        gl.glVertexPointer(3, GL.GL_FLOAT, 0, 0);
        gl.glColorPointer(3, GL.GL_FLOAT, 0, ePos.length * Float.BYTES);
        gl.glNormalPointer(GL.GL_FLOAT, 0, (ePos.length + eCol.length) * Float.BYTES);
        
        for (int i = 0; i < myTerrain.enemies().size(); i++ ) {
        	gl.glDrawArrays(GL2.GL_TRIANGLE_STRIP, (i* maxStacks * (maxSlices+1) * 2), (maxStacks * (maxSlices+1) * 2)); 
        }
        
        //gl.glBindBuffer(GL2.GL_ELEMENT_ARRAY_BUFFER, bufferIds[1]);  
        //gl.glDrawElements(GL2.GL_TRIANGLES, 6, GL2.GL_UNSIGNED_SHORT,0);        	   	
    	gl.glUseProgram(0);
        
        gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
    	gl.glDisableClientState(GL2.GL_COLOR_ARRAY);
    	gl.glDisableClientState(GL2.GL_NORMAL_ARRAY);
    	gl.glBindBuffer(GL.GL_ARRAY_BUFFER,0);
	}
	
	//Draws the enemies (using immediate calls - for testing)
	public void drawEnemies(GL2 gl) {
		
		//Set lighting properties
		float[] ambdiff = {1f, 0.2f, 0f, 1.0f};
		float[] spec = {0.7f, 0f, 0f, 1.0f};
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, ambdiff, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, spec, 0);
		
		GLUT glut = new GLUT();
		int numSegments = 32;
		
		for (Enemy e : this.myTerrain.enemies()) {
			
			gl.glPushMatrix();
			
			double[] position = e.getPosition();
			gl.glTranslated(position[0], position[1], position[2]);
			gl.glRotated(90, 1, 0, 0);
			
			glut.glutSolidTorus(0.1, 0.2, numSegments, numSegments);
			
			gl.glPopMatrix();
		}
	}
	
	//Calculates the face normal of a triangle using the cross product of 2 sides
	public double[] calcFaceNormal(double[] A, double[] B, double[] C) {
		double[] u = new double[3];
		double[] v = new double[3];
		double[] n = new double[3];
		
		//Calculate the vectors that represent 2 sides (from A)
		u[0] = B[0] - A[0];
		u[1] = B[1] - A[1];
		u[2] = B[2] - A[2];
		
		v[0] = C[0] - A[0];
		v[1] = C[1] - A[1];
		v[2] = C[2] - A[2];
		
		//Calculate the cross product to find a normal
		n[0] = u[1]*v[2] - u[2]*v[1]; 
		n[1] = -(u[0]*v[2] - u[2]*v[0]);
		n[2] = u[0]*v[1] - u[1]*v[0];
		
		return normalise(n);
	}
	
	//Calculates the newell normal of a triangle
	public double[] calcNewellNormal(double[] A, double[] B, double[] C) {
		double[] n = new double[]{0,0,0};
		
		//A
		n[0] += (A[1] - B[1]) * (A[2] + B[2]);
		n[1] += (A[2] - B[2]) * (A[0] + B[0]);
		n[2] += (A[0] - B[0]) * (A[1] + B[1]);
		
		//B
		n[0] += (B[1] - C[1]) * (B[2] + C[2]);
		n[1] += (B[2] - C[2]) * (B[0] + C[0]);
		n[2] += (B[0] - C[0]) * (B[1] + C[1]);
		
		//C
		n[0] += (C[1] - A[1]) * (C[2] + A[2]);
		n[1] += (C[2] - A[2]) * (C[0] + A[0]);
		n[2] += (C[0] - A[0]) * (C[1] + A[1]);
		
		return normalise(n);
	}
	
	//Calculates the normal of a triangle in the terrain using surrounding values
	public double[] calcTerrainNormal(double[] points) {
		double[] n = new double[3];
		
		//Calculation - Y value is average of surrounding values
		
		//Normalisation of 
		
		return normalise(n);
	}
	
	//Normalises a vector
	public double[] normalise(double[] v) {
		double[] n = new double[3];
		
		double length = Math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
		
		//Handle division by zero case
		if (length == 0) {
			return new double[]{0,0,0};
		}
		
		n[0] = v[0]/length;
		n[1] = v[1]/length;
		n[2] = v[2]/length;
		
		return n;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		 	case KeyEvent.VK_UP:
		 		//Need to use inverse transformation (-camDir) for camera translation
		 		camPos[0] += (-camDir[0] * distancestep);
		 		camPos[1] += (-camDir[1] * distancestep);
		 		camPos[2] += (-camDir[2] * distancestep);
		 		break;
		 	case KeyEvent.VK_DOWN:
		 		//Need to use inverse transformation (-camDir) for camera translation
		 		camPos[0] -= (-camDir[0] * distancestep);
		 		camPos[1] -= (-camDir[1] * distancestep);
		 		camPos[2] -= (-camDir[2] * distancestep);
		 		break;
		 	case KeyEvent.VK_RIGHT:
		 		angleXZ += anglestep;
		 		if (angleXZ > Math.PI) {
		 			angleXZ = -Math.PI + (angleXZ - Math.PI);
		 		}
		 		camDir[0] = Math.cos(angleXZ);
		 		camDir[1] = 0;
		 		camDir[2] = Math.sin(angleXZ); 
		 		
		 		camDir = normalise(camDir);
		 		break;
		 	case KeyEvent.VK_LEFT:
		 		angleXZ -= anglestep;
		 		if (angleXZ <= -Math.PI) {
		 			angleXZ = Math.PI - (Math.PI + angleXZ);
		 		}
		 		camDir[0] = Math.cos(angleXZ);
		 		camDir[1] = 0;
		 		camDir[2] = Math.sin(angleXZ); 
		 		
		 		camDir = normalise(camDir);
		 		break;
		 	case KeyEvent.VK_L:
		 		lighting = !lighting;
		 		break;
		 	case KeyEvent.VK_S:
		 		sun = !sun;
		 		break;
		 	case KeyEvent.VK_A:
		 		ambient = !ambient;
		 		break;
		 	case KeyEvent.VK_B:
		 		backFaces = !backFaces;
		 		break;
		 	case KeyEvent.VK_C:
		 		avatar = !avatar;
		 		break;
		 	case KeyEvent.VK_N:
		 		night = !night;
		 		break;
		 	case KeyEvent.VK_T:
		 		torch = !torch;
		 		break;
		 	case KeyEvent.VK_PAGE_UP:
		 		camPos[1] += distancestep;
		 		break;
		 	case KeyEvent.VK_PAGE_DOWN:
		 		camPos[1] -= distancestep;
		 		break;
		 	default:
		 		break;
		 }	
		
		//Check if inside the grid
 		if ((camPos[0] >= 0f && camPos[0] <= myTerrain.size().width) && (camPos[2] >= 0f && camPos[2] <= myTerrain.size().height)) {
 			//Check if camera is below the terrain (y axis), and move up if it is
	 		if (camPos[1] < (myTerrain.altitude(camPos[0]-camDir[0]*nearPlane, camPos[2]-camDir[2]*nearPlane) + 0.1)) {
	 			camPos[1] = myTerrain.altitude(camPos[0]-camDir[0]*nearPlane, camPos[2]-camDir[2]*nearPlane) + 0.1;
	 		}
 		}

 		torchDir[0] = (float)-camDir[0];
 		torchDir[1] = (float)-camDir[1];
 		torchDir[2] = (float)-camDir[2];
 		
 		torchPos[0] = (float)(camPos[0] + (camDir[0]*avatarDist));
 		torchPos[1] = (float)(camPos[1] + (camDir[1]*avatarDist));
 		torchPos[2] = (float)(camPos[2] + (camDir[2]*avatarDist));
		
    }

	@Override
	public void keyReleased(KeyEvent arg0) {
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		Point p = e.getPoint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {

	}
}
